/* 1 */db.users.insertMany([

{
    "_id" : ObjectId("621f268a0ada95273a69f73d"),
    "firstName" : "Jane",
    "lastName" : "Doe",
    "age" : 21.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "janedoe@gmail.com"
    },
    "courses" : [ 
        "CSS", 
        "Javascript", 
        "Python"
    ],
    "department" : "HR",
    "isAdmin" : false,
    "status" : "active"
},

/* 2 */
{
    
    "firstName" : "Stephen",
    "lastName" : "Hawking",
    "age" : 76.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "stephenhawking@gmail.com"
    },
    "courses" : [ 
        "Python", 
        "React", 
        "PHP"
    ],
    "department" : "HR",
    "status" : "active"
},

/* 3 */
{
    
    "firstName" : "Neil",
    "lastName" : "Armstrong",
    "age" : 82.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "neilarmstrong@gmail.com"
    },
    "courses" : [ 
        "React", 
        "Laravel", 
        "Sass"
    ],
    "department" : "HR",
    "status" : "active"
},

/* 4 */
{
    
    "firstName" : "Bill",
    "lastName" : "Gates",
    "age" : 65.0,
    "contact" : {
        "phone" : "12345678",
        "email" : "bill@gmail.com"
    },
    "courses" : [ 
        "PHP", 
        "Laravel", 
        "HTML"
    ],
    "department" : "Operations",
    "status" : "active"
}
])























// sol 1

db.users.find(
		{
			$or: [{"firstName": /S/}, 
            {"lastName": /D/}]
		}, 
		{
			"firstName":1,
			"lastName": 1,
			"_id": 0
		}
	)

// sol 2
db.users.find(
	{
		$and:[
			{department : "HR"},
			{age : {$gte:70}},
		]
	}




	)

// sol 3
db.users.find(
	{
		$and:
		[

		{
			age : {$lte:30}
		},
		{firstName:{$regex: /e/,
		$options: 'i' 
		}}
		]
	}	
)


